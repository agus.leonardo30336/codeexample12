package com.example.codeexample12

import android.content.Context
import android.content.SharedPreferences

class SizeSharedPref(context: Context) {
    private val sizeKey ="KeyRand"
    private var myPreferences : SharedPreferences = context.getSharedPreferences("sharePrefSize",Context.MODE_PRIVATE)

    var size: Int
        get() = myPreferences.getInt(sizeKey,0)
        set(value) {
            if(value<=0)
                myPreferences.edit().putInt(sizeKey,0).apply()
            else
                myPreferences.edit().putInt(sizeKey,value).apply()
        }
}

