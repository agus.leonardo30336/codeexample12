package com.example.codeexample12

import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var mySharedPref: SizeSharedPref
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mySharedPref = SizeSharedPref(this)
        plusBtn.setOnClickListener {
            mySharedPref.size += 1
            size.text = mySharedPref.size.toString()
        }
        minBtn.setOnClickListener {
            mySharedPref.size -= 1
            size.text = mySharedPref.size.toString()
        }
    }
    override fun onResume() {
        super.onResume()
        size.text = mySharedPref.size.toString()
    }

    override fun onStop() {
        super.onStop()
        val appWidgetManager = AppWidgetManager.getInstance(this)
        val ids = appWidgetManager.getAppWidgetIds(
            ComponentName(this, SizeWidget::class.java)
        )
        val updateIntent = Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE)
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
        sendBroadcast(updateIntent)
    }
}



