package com.example.codeexample12

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import java.util.*

class UpdateMyWidget(context: Context) {
    private val inContext = context
    private val ALARM_ID = 0
    private val INTERVAL_MILLIS = 60000L

    fun startAlarm() {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MILLISECOND, INTERVAL_MILLIS.toInt())

        val alarmIntent = Intent(inContext, MyMessage::class.java)
        alarmIntent.action = MyMessage.ACTION_AUTO_UPDATE
        val pendingIntent = PendingIntent.getBroadcast(
            inContext,
            ALARM_ID, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT
        )

        val alarmManager = inContext.getSystemService(Context.ALARM_SERVICE)
                as AlarmManager
        alarmManager.setRepeating(
            AlarmManager.RTC, calendar.timeInMillis,
            INTERVAL_MILLIS, pendingIntent
        )
    }

    fun stopAlarm() {
        val alarmIntent = Intent(inContext, MyMessage::class.java)
        alarmIntent.action = MyMessage.ACTION_AUTO_UPDATE
        val pendingIntent = PendingIntent.getBroadcast(
            inContext, ALARM_ID,
            alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT
        )

        val alarmManager = inContext.getSystemService(Context.ALARM_SERVICE)
                as AlarmManager
        alarmManager.cancel(pendingIntent)
    }
}

