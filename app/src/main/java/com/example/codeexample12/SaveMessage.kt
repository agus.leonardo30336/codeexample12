package com.example.codeexample12

class SaveMessage() {
    var lstMessage = arrayListOf(
        "Hello", "Have a nice Day",
        "Sunny Day", "Remember to be Happy"
    )
    private var index = -1
    fun addMessage(srt: String) {
        lstMessage.add(srt)
    }

    fun removeMessage(srt: String) {
        lstMessage.remove(srt)
    }

    fun backToStart() {
        index = -1
    }

    fun getMessage(): String {
        if (index + 1 == lstMessage.size)
            backToStart()
        index += 1
        return lstMessage.get(index)
    }
}

