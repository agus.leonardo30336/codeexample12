package com.example.codeexample12

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews

/**
 * Implementation of App Widget functionality.
 */
class SizeWidget : AppWidgetProvider() {

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
    }

    override fun onDisabled(context: Context) {
    }

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)
        if (AddOnClick == intent.action) {
            val sizePref = SizeSharedPref(context)
            sizePref.size += 1
        } else if (MinOnClick == intent.action) {
            val sizePref = SizeSharedPref(context)
            sizePref.size -= 1
        }
        val appWidgetManager = AppWidgetManager.getInstance(context)
        val thisAppWidgetComponentName = ComponentName(context.packageName, javaClass.name)
        val appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidgetComponentName)
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }

    }

    companion object {
        private val AddOnClick = "AddOnClick"
        private val MinOnClick = "MinOnClick"
        internal fun updateAppWidget(
            context: Context,
            appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ) {
            val views = RemoteViews(context.packageName, R.layout.size_widget)
            val sizePref = SizeSharedPref(context)
            views.setTextViewText(
                R.id.appwidget_text_size,
                sizePref.size.toString()
            )

            views.setOnClickPendingIntent(
                R.id.appwidget_btn_plus,
                getPendingSelfIntent(context, AddOnClick)
            )

            views.setOnClickPendingIntent(
                R.id.appwidget_btn_min,
                getPendingSelfIntent(context, MinOnClick)
            )

            appWidgetManager.updateAppWidget(appWidgetId, views)
        }


        private fun getPendingSelfIntent(
            context: Context,
            buttonStr: String
        ): PendingIntent {
            val intent = Intent(context, SizeWidget::class.java)
            intent.action = buttonStr

            return PendingIntent.getBroadcast(context, 0, intent, 0)
        }
    }
}


