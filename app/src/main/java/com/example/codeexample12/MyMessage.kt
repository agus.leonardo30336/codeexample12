package com.example.codeexample12

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews

/**
 * Implementation of App Widget functionality.
 */
class MyMessage : AppWidgetProvider() {

    override fun onUpdate(context: Context,
                          appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }
    override fun onEnabled(context: Context) {
        val updateMyWidget = UpdateMyWidget(context.applicationContext)
        updateMyWidget.startAlarm()
    }
    override fun onDisabled(context: Context) {
        val appWidgetManager = AppWidgetManager.getInstance(context);
        val thisAppWidgetComponentName = ComponentName(context.getPackageName()
            ,javaClass.getName());
        val appWidgetIds = appWidgetManager
            .getAppWidgetIds(thisAppWidgetComponentName);
        if (appWidgetIds.size == 0) {
            val appWidgetAlarm = UpdateMyWidget(context.getApplicationContext());
            appWidgetAlarm.stopAlarm();
        }
    }
    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)
        if(intent.action.equals(MyMessage.ACTION_AUTO_UPDATE))
        {
            val appWidgetManager = AppWidgetManager.getInstance(context);
            val thisAppWidgetComponentName = ComponentName(context.getPackageName()
                ,javaClass.getName());
            val appWidgetIds = appWidgetManager
                .getAppWidgetIds(thisAppWidgetComponentName);
            for (appWidgetId in appWidgetIds) {
                updateAppWidget(context, appWidgetManager, appWidgetId)
            }
        }
    }
    companion object {
        val ACTION_AUTO_UPDATE = "AUTO_UPDATE"
        private val Message = SaveMessage()
        internal fun updateAppWidget(context: Context,
                                     appWidgetManager: AppWidgetManager,
                                     appWidgetId: Int) {
            val widgetText = Message.getMessage()
            val views = RemoteViews(context.packageName, R.layout.my_message)
            views.setTextViewText(R.id.appwidget_text, widgetText)
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }
}




